package com.insy2s.E_REDUX.Product;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.insy2s.E_REDUX.Entites.*;
import com.insy2s.E_REDUX.Repository.CategoryRepository;
import com.insy2s.E_REDUX.Repository.ProductRepository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;

@DataJpaTest(showSql = true)
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)

public class ProductTestsRepository {
	@Autowired
	private ProductRepository repo;
	@Autowired
	private CategoryRepository categoryRepository;
	/*
	 * @Test public void testCreateProduct() { Optional<Category>
	 * category=categoryRepository.findById(2); Product product=new
	 * Product("xx",3.2,"rbrb",category.get()); Product
	 * resultProduct=repo.save(product); The assertThat is one of the JUnit methods
	 * from the Assert object that can be used to check if a specific value match to
	 * an expected one. }
	 */
}
