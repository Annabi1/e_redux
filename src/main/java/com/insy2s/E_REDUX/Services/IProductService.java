package com.insy2s.E_REDUX.Services;

import java.util.List;
import java.util.Optional;

import com.insy2s.E_REDUX.Entites.Product;

public interface IProductService {
	public List<Product> findAll();
	public Product save(Product product);
	public void delete(Integer id);
	public Optional<Product> getProductById(Integer id);
    public Product updateProduct(Product product);

}
