package com.insy2s.E_REDUX.Controller;


	import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.insy2s.E_REDUX.Entites.Product;
import com.insy2s.E_REDUX.Services.ProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


	@RestController
	@RequestMapping("/api/product")
	@CrossOrigin(origins = "*")


	public class ProductController {
		 private final Logger log = LoggerFactory.getLogger(Product.class);


		@Autowired
		private ProductServiceImpl productServiceImpl;
		
		@GetMapping("/")
		   public List<Product> getAllProducts( )  {
		        log.debug("REST request to get all  Products : {}");
		           return productServiceImpl.findAll();
		    }
		@PostMapping(path = "/", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE  })
		   public ResponseEntity<Product> createProduct( @RequestBody Product product,@RequestPart("photo") MultipartFile multipartFile) throws IOException {
		         
		        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		        product.setImage(fileName);
		        Product result = productServiceImpl.save(product);

		 
		        String uploadDir = "products-photos/" + result.getProductId();
		 
		        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
		        
		        return ResponseEntity.status(201)
		                .body(result);
		    }

	}

